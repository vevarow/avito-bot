#!/usr/bin/env python3

from bs4 import BeautifulSoup
from time import sleep
from random import randrange
import sys
import argparse
import urllib.request
import urllib.parse
from urllib.error import URLError
from urllib.error import HTTPError

CATALOG_SELECTOR = 'description'
PRICES_SELECTOR = 'popup-prices popup-prices__wrapper clearfix'

class TG:
	def __init__(self, token, chat_id, sleep_time):
		self.token = token
		self.base_url = "https://api.telegram.org/bot" + self.token + "/"
		self.chat_id = chat_id
		self.sleep_time = sleep_time

	def _send(self, method, params, **kwargs):
		url = kwargs['base_url'] if 'base_url' in kwargs else self.base_url
		url = url + method
		while True:
			try:
				if len(params):
					return urllib.request.urlopen(url, urllib.parse.urlencode(params).encode('utf-8')).read()
				else:
					return urllib.request.urlopen(url).read()
			except URLError as e:
				print("URLError occurred during request", url, e.reason)
				sleep(self.sleep_time)
			except HTTPError as e:
				print("HTTPError occurred during request", url, e.reason)
				sleep(self.sleep_time)
			except:
				print("Something went wrong for", url)
				sleep(self.sleep_time)

	def send(self, msg):
		print('Message: ', self._send('sendMessage', { "chat_id": self.chat_id, "text": msg }))

	def form_msg(self, soup):
		base = soup
		res = ''
		res = res + str(base.h3.a['title'])
		res = res + str(base.find('div', 'about').text)
		res = res + "http://avito.ru" + str(base.h3.a['href'])

		return res

	def send_set(self, current_set):
		if not len(current_set):
			print('same')
			return

		for i in range(0, len(current_set)):
			self.send(self.form_msg(current_set[i]))

def main(token, max_sleep_time, chat_id, base_url):
	ethalon = []
	tgbot = TG(token, chat_id, max_sleep_time)

	counter = 0

	while True:
		counter = counter + 1
		print('Checking avito...\ncounter =', counter)

		html = BeautifulSoup(tgbot._send('', {}, base_url=base_url), "html.parser")
		elements = html.find_all('div', CATALOG_SELECTOR)

		for i in range(0, len(elements)):
			try:
				del elements[i].find('div', PRICES_SELECTOR)['data-prices']
			except TypeError:
				pass

			try:
				del elements[i].find('div', PRICES_SELECTOR)['data-currencies']
			except TypeError:
				pass

		sleep_time = randrange(max_sleep_time / 2, max_sleep_time)
		print('next sleep time =', sleep_time, '\n')

		if not len(ethalon):
			ethalon = elements

		tgbot.send_set(list(set(elements) - set(ethalon)))
		
		ethalon = elements

		sleep(sleep_time)


if __name__ == "__main__":
	parser = argparse.ArgumentParser(
		description="This tool is used for parsing avito.com and instant notification of new flats arrivals"
	)
	parser.add_argument(
		"--chatId",
		help="the id of Telegram chat",
		required=True
	)
	parser.add_argument(
		"--telegramToken",
		help="telegram token",
		required=True
	)
	parser.add_argument(
		"--baseUrl",
		help="base url for avito page with filter",
		required=True
	)
	parser.add_argument(
		"--maxSleepTime",
		help="max time between avito checks",
		type=int,
		default=30,
		required=False
	)
	args = vars(parser.parse_args())

	main(args["telegramToken"], args["maxSleepTime"], args["chatId"], args["baseUrl"])
